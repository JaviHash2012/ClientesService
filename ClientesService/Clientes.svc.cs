﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ClientesService
{
    public class Clientes : IClientes
    {
        private HelperCliente helper;

        /// <summary>
        /// Da de alta un nuevo cliente
        /// </summary>
        /// <param name="cliente"> Cliente a ser dado de alta</param>
        /// <returns> El nuevo cliente</returns>
        public Cliente Create(Cliente cliente)
        {
            helper = new HelperCliente();
            return helper.CrearCliente(cliente) ?? new Cliente();
        }

        /// <summary>
        /// Elimina un cliente seleccionado
        /// </summary>
        /// <param name="cliente"> Informacion del cliente a eliminar</param>
        public void Delete(Cliente cliente)
        {
            helper = new HelperCliente();
            helper.EliminarCliente(cliente);
        }

        /// <summary>
        /// Obtiene un cliente por su ID
        /// </summary>
        /// <param name="cliente"> ID del Cliente</param>
        /// <returns> El cliente solicitado </returns>
        public Cliente Get(string id_cliente)
        {
            Cliente cliente = new Cliente
            {
                Id_cliente = int.Parse(id_cliente)
            };
            helper = new HelperCliente();
            return helper.ObtenerCliente(cliente) ?? new Cliente();
        }

        /// <summary>
        /// Obtiene todos los registros de la tabla clientes
        /// </summary>
        /// <returns> La lista de los clientes </returns>
        public List<Cliente> GetAll()
        {
            helper = new HelperCliente();
            return helper.ObtenerClientes() ?? new List<Cliente>();
        }

        /// <summary>
        ///  Actualiza la informacion de un cliente solicitado
        /// </summary>
        /// <param name="cliente"> Informacion a actualizar</param>
        /// <returns> Cliente actualizado </returns>
        public Cliente Update(Cliente cliente)
        {
            helper = new HelperCliente();
            return helper.ActualizarCliente(cliente) ?? new Cliente();
        }
    }

}
