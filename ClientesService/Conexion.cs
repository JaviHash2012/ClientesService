﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientesService
{
    public class Conexion
    {
        /// <summary>
        ///Variables globales
        /// </summary>
        private string database;
        private string userId;
        private string password;
        private string server;

        private MySqlConnectionStringBuilder cadenaConexion;

        /// <summary>
        /// Inicializa las variables para la conexion
        /// </summary>
        public Conexion()
        {
            database = "clientes";
            userId = "root";
            password = "ISWHash2012Creative1220";
            server = "localhost";
        }

        /// <summary>
        /// Crea la conexion a la base de datos
        /// </summary>
        /// <returns>Objeto Conwxion</returns>
        public MySqlConnection Conectar()
        {
            cadenaConexion = new MySqlConnectionStringBuilder
            {
                Database = database,
                UserID = userId,
                Password = password,
                Server = server
            };
            return new MySqlConnection(cadenaConexion.ToString());
        }
    }
}