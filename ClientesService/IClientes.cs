﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ClientesService
{
    [ServiceContract]
    public interface IClientes
    {
        [OperationContract]
        [WebGet(UriTemplate = "/getAll", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Cliente> GetAll();

        [OperationContract]
        [WebGet(UriTemplate = "/get/{id_cliente}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Cliente Get(string id_cliente);

        [OperationContract]
        [WebInvoke(UriTemplate = "/create", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Cliente Create(Cliente cliente);

        [OperationContract]
        [WebInvoke(UriTemplate = "/update", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, Method = "PUT")]
        Cliente Update(Cliente cliente);

        [OperationContract]
        [WebInvoke(UriTemplate = "delete", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, Method = "DELETE")]
        void Delete(Cliente cliente);

    }

    [DataContract]
    public class Cliente
    {
        /// <summary>
        /// Propiedades de la clase cliente
        /// </summary>
        [DataMember]
        public int Id_cliente
        {
            get;
            set;
        }
        [DataMember]
        public string Nombre
        {
            get;
            set;
        }
        [DataMember]
        public string Apellido_paterno
        {
            get;
            set;
        }
        [DataMember]
        public string Apellido_materno
        {
            get;
            set;
        }
        [DataMember]
        public string Telefono
        {
            get;
            set;
        }
        [DataMember]
        public string Calle
        {
            get;
            set;
        }
        [DataMember]
        public int No_interior
        {
            get;
            set;
        }
        [DataMember]
        public int No_exterior
        {
            get;
            set;
        }
        [DataMember]
        public string Colonia
        {
            get;
            set;
        }
        [DataMember]
        public string Ciudad
        {
            get;
            set;
        }
        [DataMember]
        public string Estado
        {
            get;
            set;
        }

    }

}
