﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientesService
{
    public class HelperCliente
    {
        private Conexion conector;
        private List<Cliente> clientes;
        private Cliente clienteBD;

        private MySqlConnection conexion;
        private MySqlCommand comando;
        private MySqlDataReader reader;

        public List<Cliente> ObtenerClientes()
        {
            conector = new Conexion();
            clientes = new List<Cliente>();
            using (conexion = conector.Conectar())
            {
                try
                {
                    conexion.Open();
                    comando = new MySqlCommand("SELECT * FROM Clientes;", conexion);
                    reader = comando.ExecuteReader();

                    while (reader.Read())
                    {
                        clientes.Add(new Cliente
                        {
                            Id_cliente = reader.GetInt32("id_cliente"),
                            Nombre = reader.GetString("nombre"),
                            Apellido_paterno = reader.GetString("apellido_paterno"),
                            Apellido_materno = reader.GetString("apellido_materno"),
                            Telefono = reader.GetString("telefono"),
                            Calle = reader.GetString("calle"),
                            No_interior = reader.GetInt32("no_int"),
                            No_exterior = reader.GetInt32("no_ext"),
                            Colonia = reader.GetString("colonia"),
                            Ciudad = reader.GetString("ciudad"),
                            Estado = reader.GetString("estado")

                        });
                    }
                    conexion.Close();
                    return clientes;
                }
                catch (MySqlException ex)
                {
                    return null;
                }
            }
        }

        public Cliente ObtenerCliente(Cliente cliente)
        {
            conector = new Conexion();

            using (conexion = conector.Conectar())
            {
                try
                {
                    conexion.Open();
                    comando = new MySqlCommand("SELECT * FROM Clientes WHERE id_cliente = "+cliente.Id_cliente+"",conexion);
                    reader = comando.ExecuteReader();
                    while (reader.Read())
                    {
                        clienteBD = new Cliente
                        {
                            Id_cliente = reader.GetInt32("id_cliente"),
                            Nombre = reader.GetString("nombre"),
                            Apellido_paterno = reader.GetString("apellido_paterno"),
                            Apellido_materno = reader.GetString("apellido_materno"),
                            Telefono = reader.GetString("telefono"),
                            Calle = reader.GetString("calle"),
                            No_interior = reader.GetInt32("no_int"),
                            No_exterior = reader.GetInt32("no_ext"),
                            Colonia = reader.GetString("colonia"),
                            Ciudad = reader.GetString("ciudad"),
                            Estado = reader.GetString("estado")
                        };
                    }
                    conexion.Close();
                    return clienteBD;
                }
                catch (MySqlException ex)
                {
                    return null;
                }

            }
        }

        public Cliente ActualizarCliente(Cliente cliente)
        {
            conector = new Conexion();

            using (conexion = conector.Conectar())
            {
                try
                {
                    conexion.Open();
                    comando = new MySqlCommand("UPDATE Clientes SET nombre = '"+cliente.Nombre+"', apellido_paterno = '"+cliente.Apellido_paterno+"'," +
                                               "apellido_materno = '"+cliente.Apellido_materno+"', telefono = '"+cliente.Telefono+"', calle = '"+cliente.Calle+"'," +
                                               "no_int = '"+cliente.No_interior+"', no_ext = '"+cliente.No_exterior+"', colonia = '"+cliente.Colonia+"'," +
                                               "ciudad = '"+cliente.Ciudad+"', estado = '"+cliente.Estado+"' WHERE id_cliente = "+cliente.Id_cliente+"", conexion);;
                    comando.ExecuteNonQuery();
                    conexion.Close();
                    return ObtenerCliente(cliente);
                }
                catch (MySqlException ex)
                {
                    return null;
                }
            }
        }

        public void EliminarCliente(Cliente cliente)
        {
            conector = new Conexion();
            using (conexion = conector.Conectar())
            {
                try
                {
                    conexion.Open();
                    comando = new MySqlCommand("DELETE FROM Clientes WHERE id_cliente = "+cliente.Id_cliente+"", conexion);
                    comando.ExecuteNonQuery();
                    conexion.Close();
                }
                catch (MySqlException ex) { }
            }
        }

        public Cliente CrearCliente(Cliente cliente)
        {
            conector = new Conexion();

            using (conexion = conector.Conectar())
            {

                try
                {
                    conexion.Open();
                    comando = new MySqlCommand("INSERT INTO Clientes(nombre, apellido_paterno," +
                                                "apellido_materno, telefono, calle, no_int, no_ext," +
                                                "colonia, ciudad, estado) VALUES('"+cliente.Nombre+"','"+cliente.Apellido_paterno+"','"+cliente.Apellido_materno+"'," +
                                                "'"+cliente.Telefono+"', '"+cliente.Calle+"',"+cliente.No_interior+","+cliente.No_exterior+", '"+cliente.Colonia+"', '"+cliente.Ciudad+"', '"+cliente.Estado+"');", conexion);
                    comando.ExecuteNonQuery();
                    comando = new MySqlCommand("SELECT LAST_INSERT_ID()", conexion);
                    clienteBD = ObtenerCliente(new Cliente
                    {
                        Id_cliente = comando.ExecuteNonQuery()
                    });
                    conexion.Close();
                    return clienteBD;
                }
                catch (MySqlException ex)
                {
                    return null;
                }
            }
        }

    }
}